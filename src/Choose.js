import React from 'react';
import { Container, Box, Button } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

import StyledAppBar from './components/topAppBar/topAppBar.component';
import Select from './components/select/select.component'

const useStyles = makeStyles(theme => ({
    img: {
        width: '600px',
        height: '440',
    },
    box: {
        marginTop: '200px',
        marginBottom: theme.spacing(5),
    },
    buttonBox: {
        margin: '50px',
    },
    button: {
        backgroundColor: '#66bb6a', 
        color: 'white',
        width: '200px',
    }
}));


export default function Escolha() {
    const classes = useStyles();

    return (
        <div className="Escolha">
            <StyledAppBar />
                <Container > 
                    <Grid
                    container
                    direction="row"
                    justify="center"
                    alignItems="center">
                        <Grid item xs={4}>
                            <Typography variant="h4" component="h4">
                                Ajude-nos a escolher as Organizações que serão parte do VOA
                            </Typography>
                        </Grid>
                        <Grid item xs={2}>
                        </Grid>
                        <Grid item xs={6}>
                        <img alt="astronaut"
                            className={classes.img}
                            src={`https://images.unsplash.com/photo-1569013636299-8219e8ce6cdd?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1267&q=80`} />
                        </Grid>
                    </Grid>
                <Box 
                    textAlign="center" 
                    className={classes.box}
                    justify="center"
                    alignItems="center"
                >
                    <Typography variant="h4" component="h4"> 
                        Pronto para começar?
                    </Typography>
                </Box>
                <Box
                    textAlign="center"
                    // className={classes.box}
                    justify="center"
                    alignItems="center"
                >
                    <Select />
                </Box>
                <Box
                    textAlign="center"
                    className={classes.buttonBox}
                    justify="center"
                    alignItems="center"
                >
                    <Button 
                        variant="contained"
                         size="large" 
                         className={classes.button}
                    >
                        COMEÇAR
                    </Button>
                </Box>
                </Container>
        </div>
    )
}
