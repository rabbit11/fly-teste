import React, { Component } from 'react';
import { CardList } from './components/card-list/card-list.component';
import { Container, Box } from '@material-ui/core';
import StyledAppBar from './components/topAppBar/topAppBar.component';
import SearchBar from './components/searchBar/searchBar.component';

import './App.css';

class App extends Component {
  constructor() {
    super();

    this.state = {
      monsters: [],
      searchField: '',
      videoUrl: ''
    };

  }

  componentDidMount() {
    fetch('https://jsonplaceholder.typicode.com/users')
    .then(response => response.json())
    .then(users => this.setState({monsters: users}));
  }

  handleChange = (e) => {
    this.setState({ searchField: e.target.value });
  }
  
  render() {
    const { monsters, searchField} = this.state;
    const filteredMonsters = monsters.filter(monster =>
      monster.name.toLowerCase().includes(searchField.toLowerCase())
    );
    const videoUrl = "https://youtu.be/vhPhZbO7OXE";

    return (
      <div className="App">
        <StyledAppBar/>

        <Container maxWidth="md">
            <h1>VEJA O PERFIL DAS ORGANIZAÇÕES E ESCOLHA 1 DELAS PARA VOTAR</h1>
            <Box display="flex" justifyContent="center" alignItems="center">
              <SearchBar
                placeholder={'Procure por uma OSC'}
                handleChange={this.handleChange}
                />
            </Box>
            <CardList 
              monsters={filteredMonsters}
              videoUrl={videoUrl}
            />
        </Container>
      </div>
    );
  }
}

export default App;
