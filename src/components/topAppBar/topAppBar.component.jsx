import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { Button } from '@material-ui/core/';
import IconButton from '@material-ui/core/IconButton';
import { Container, Link } from '@material-ui/core';
import { styled } from '@material-ui/core/styles';
// import MenuIcon from '@material-ui/icons/Menu';

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
}));

const StyledTopAppBar = styled(AppBar)({
    background: '#2c387e',
    marginBottom: '50px',
});

const StyledLink = styled(Link)({
    padding: '0 30px',
    fontWeight: 'semi-bold'
});

const StyledButton = styled(Button)({
    backgroundColor: 'white',
    color: '#66bb6a',
});

export default function StyledAppBar() {
    return (
        <div className="StyledAppBar">
            <StyledTopAppBar position="sticky">
                <Container>
                    <Toolbar>
                        <StyledLink href="#" color="inherit">Início</StyledLink>
                        <StyledLink href="#" color="inherit">Como Funciona</StyledLink>
                        <StyledLink href="#" color="inherit">Quem Somos</StyledLink>
                        <StyledLink href="#" color="inherit">Instituições Participantes</StyledLink>
                        <StyledLink href="#" color="inherit">Inscreva sua Organização</StyledLink>
                        <StyledButton size="small" variant="contained">Log in</StyledButton>
                    </Toolbar>
                </Container>
            </StyledTopAppBar>
        </div>
    );
}
