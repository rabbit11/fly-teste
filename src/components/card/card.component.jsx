import React from 'react';
import TransitionsModal from '../modal/modal.component';
import ReactPlayer from 'react-player';

import './card.styles.css';


export const Card = props => (
    <div className='card-container'>
    {console.log(props.videoUrl)}
        <div className='player-wrapper'> 
            <ReactPlayer 
                className='react-player'
                url={props.videoUrl}
                width='100%'
                height='100%' 
                controls={false}
                light={true}
            />
        </div>
        <h1>{props.monster.name}</h1>
        <p><span>História </span> {props.monster.company.catchPhrase}</p>
        <p><span>Missão </span> {props.monster.company.bs}</p>
        <p><span>Dados </span> {props.monster.company.website}</p>
        <p><span>Contatos </span> {props.monster.email}</p>

        <TransitionsModal> </TransitionsModal>
    </div>
)