import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const useStyles = makeStyles(theme => ({
    formControl: {
        // margin: theme.spacing(1),
        width: 1018,
        height: 70,
        marginTop: theme.spacing(3),

    },
    selectEmpty: {
        // marginTop: theme.spacing(2),
    },
}));

export default function SimpleSelect() {
    const classes = useStyles();
    const [age, setAge] = React.useState('');

    const inputLabel = React.useRef(null);
    const [labelWidth, setLabelWidth] = React.useState(0);
    React.useEffect(() => {
        setLabelWidth(inputLabel.current.offsetWidth);
    }, []);

    const handleChange = event => {
        setAge(event.target.value);
    };

    return (
        <div>
            <FormControl variant="outlined" className={classes.formControl}>
                <InputLabel ref={inputLabel} className="demo-simple-select-outlined-label">
                    ESCOLHA O ESTADO DA ORGANIZAÇÃO
                </InputLabel>
                <Select
                    labelId="demo-simple-select-outlined-label"
                    className="demo-simple-select-outlined"
                    value={age}
                    onChange={handleChange}
                    labelWidth={labelWidth}
                >
                    <MenuItem value={10}>SP</MenuItem>
                    <MenuItem value={20}>RJ</MenuItem>
                    <MenuItem value={30}>MG</MenuItem>
                </Select>
            </FormControl>
            <FormControl variant="outlined" className={classes.formControl}>
                <InputLabel ref={inputLabel} className="demo-simple-select-outlined-label">
                    ESCOLHA ATIVIDADE PRINCIPAL DA ORGANIZAÇÃO
                </InputLabel>
                <Select
                    labelId="demo-simple-select-outlined-label"
                    className="demo-simple-select-outlined"
                    value={age}
                    onChange={handleChange}
                    labelWidth={labelWidth}
                >
                    <MenuItem value="">
                        <em>Outro</em>
                    </MenuItem>
                    <MenuItem value={10}></MenuItem>
                    <MenuItem value={20}>RJ</MenuItem>
                    <MenuItem value={30}>MG</MenuItem>
                </Select>
            </FormControl>
        </div>
    );
}
