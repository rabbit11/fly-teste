import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import InputBase from '@material-ui/core/InputBase';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';
import DirectionsIcon from '@material-ui/icons/Directions';
import { styled } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    root: {
        padding: '2px 4px',
        display: 'flex',
        alignItems: 'center',
        width: 400,
    },
    input: {
        marginLeft: theme.spacing(1),
        flex: 1,
    },
    iconButton: {
        padding: 5,
    },
    divider: {
        height: 28,
        margin: 4,
    },
}));

const StyledBox = styled(Box)({
    margin: '50px',
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: 300,
    border:'1px solid gray',
    borderRadius: '5px',
    lineHeight: '30px',
});

export default function SearchBar({placeholder, handleChange}) {
    const classes = useStyles();

    return (
        <StyledBox 
            // border={1} 
            // borderColor="gray"
        >
            <InputBase
                className={classes.input}
                placeholder={placeholder}
                onChange={handleChange} 
                color="primary"
            />
            {/* <IconButton type="submit" className={classes.iconButton} aria-label="search"> */}
                <SearchIcon />
            {/* </IconButton> */}
        </StyledBox>
    );
}
