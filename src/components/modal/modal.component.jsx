import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import { Button, Container, TextField } from '@material-ui/core';

import EmailField from '../emailField/emailField.component';

const useStyles = makeStyles(theme => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    },
}));

export default function TransitionsModal() {
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    return (
        <div className="MyModal">
            {/* <button type="button" onClick={handleOpen}>
                Votar
            </button> */}

            <Button 
                variant="contained" 
                size="small" 
                style={{ backgroundColor: '#66bb6a', color: 'white' }} 
                onClick={handleOpen} >
                Votar
            </Button>

            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={open}
                onClose={handleClose}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 500,
                }}
            >
                <Fade in={open}>
                    <div className={classes.paper}>
                        <Container>
                            <h2 id="transition-modal-title">
                                AGRADECEMOS PELO SEU VOTO!
                            </h2>
                            <p id="transition-modal-description">AGORA É SÓ DEIXAR SEU E-MAIL E FINALIZAR</p>
                            <form className={classes.root} noValidate autoComplete="off">
                                {/* <TextField id="standard-basic" label="Email" /> */}
                                {/* <TextField id="filled-basic" label="Filled" variant="filled" /> */}
                                <TextField id="outlined-basic" label="Email" variant="outlined" />
                            </form>
                        </Container>
                    </div>
                </Fade>
            </Modal>
        </div>
    );
}
